# Monkey OS

## Homepage

[This is the main page](https://monkeylabs.gitlab.io/projects/monkeyOS.html) for the project on the internet

## Why?

Because some times you get bored of writing high level game engine code and want to just deal with the machine hardware and nothing else

## What is the end goal?

Nothing really. I don't ever imagine this being used except for my own personal entertainment and education.

One day I may push it all the way to be able to boot into it from a USB disk, but thats about it

UPDATE: Rethinking about it again and about [this awesome SMBC comic](https://www.smbc-comics.com/index.php?id=2158) the end goal is to, obviously, be able to run DOOM

## How do I try it?

make

Install Qemu and ./run.sh

Or figure out your own VM and boot from floppy with the generated floppy image "inserted"

## Should I (you) fork this? modify this? run it on my fridge?

[![License: 0BSD](https://img.shields.io/badge/License-0BSD-blue.svg)](https://opensource.org/licenses/0BSD)

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
