ASM=nasm
CC=clang
CC16=wcc
LD16=wlink
SRC_DIR=src
TOOLS_DIR=tools
BUILD_DIR=build

.PHONY: all floppy_image kernel bootloader clean always tools_fat

all: floppy_image tools_fat

################# floppy image
floppy_image: $(BUILD_DIR)/main_floppy.img

$(BUILD_DIR)/main_floppy.img : bootloader kernel
	# create empty disk image 
	dd if=/dev/zero of=$(BUILD_DIR)/main_floppy.img bs=512 count=2880
	# make fat12 filesys on the image
	mkfs.fat -F 12 -n "MONKEYOS" $(BUILD_DIR)/main_floppy.img
	# put bootloader in first sector of floppy
	dd if=$(BUILD_DIR)/blstage0.bin of=$(BUILD_DIR)/main_floppy.img conv=notrunc
	# copy bootloader second stage to the disk
	mcopy -i $(BUILD_DIR)/main_floppy.img $(BUILD_DIR)/blstage1.bin "::blstage1.bin"
	# copy kernel file to the disk
	mcopy -i $(BUILD_DIR)/main_floppy.img $(BUILD_DIR)/kernel.bin "::kernel.bin"

################## bootloader
bootloader : bootloader_stage0 bootloader_stage1

bootloader_stage0: $(BUILD_DIR)/blstage0.bin

$(BUILD_DIR)/blstage0.bin: always
	# assembling bootloader stage 0
	$(MAKE) -C $(SRC_DIR)/bootloader/0stage BUILD_DIR=$(abspath $(BUILD_DIR)) ASM=$(ASM)

bootloader_stage1: $(BUILD_DIR)/blstage1.bin

$(BUILD_DIR)/blstage1.bin: always
	# assembling bootloader stage 1
	$(MAKE) -C $(SRC_DIR)/bootloader/1stage BUILD_DIR=$(abspath $(BUILD_DIR)) ASM=$(ASM)

################## kernel
kernel : $(BUILD_DIR)/kernel.bin

$(BUILD_DIR)/kernel.bin: always
	# assembling kernel
	$(MAKE) -C $(SRC_DIR)/kernel BUILD_DIR=$(abspath $(BUILD_DIR)) ASM=$(ASM)

################## tools_fat
tools_fat : $(BUILD_DIR)/$(TOOLS_DIR)/fat

$(BUILD_DIR)/$(TOOLS_DIR)/fat: always
	# compiling FAT tools
	$(MAKE) -C $(SRC_DIR)/$(TOOLS_DIR)/fat/ BUILD_DIR=$(abspath $(BUILD_DIR)) TOOLS_DIR=$(TOOLS_DIR) CC=$(CC)

################## always
always :
	mkdir -p $(BUILD_DIR)

################## clean
clean :
	# cleaning bootloader stage0
	$(MAKE) -C $(SRC_DIR)/bootloader/0stage BUILD_DIR=$(abspath $(BUILD_DIR)) clean
	# cleaning bootloader stage1
	$(MAKE) -C $(SRC_DIR)/bootloader/1stage BUILD_DIR=$(abspath $(BUILD_DIR)) clean
	# cleaning kernel
	$(MAKE) -C $(SRC_DIR)/kernel BUILD_DIR=$(abspath $(BUILD_DIR)) clean
	# cleaning FAT tools
	$(MAKE) -C $(SRC_DIR)/$(TOOLS_DIR)/fat/ BUILD_DIR=$(abspath $(BUILD_DIR)) TOOLS_DIR=$(TOOLS_DIR) clean
	# removing all files in build dir
	rm -rf $(BUILD_DIR)/*
