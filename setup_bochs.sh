#!/bin/bash

if ! [ -d ./bochs ]; then
	echo "creating bochs directory"
	mkdir ./bochs
fi

if ! [ -f ./bochs/bios ]; then
  echo "bios not found, creating"
	ln -s /usr/share/bochs/BIOS-bochs-latest ./bochs/bios
fi

if ! [ -f ./bochs/vgabios ]; then
  echo "vgabios not found, creating"
	ln -s /usr/share/bochs/VGABIOS-lgpl-latest ./bochs/vgabios
fi
