#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* 
 * C does not have bools
 * how did this happen
 * ah well
 */
typedef uint8_t bool;
#define true 1
#define false 0

/* 
 * struct that can store the information 
 * from a boot sector from a disk
 */
typedef struct __attribute__((packed))
{
	uint8_t BootJumpInstruction[3];
	uint8_t OEMIdentifier[8];
	uint16_t BytesPerSector;
	uint8_t SectorsPerCluster;
	uint16_t ReservedSectors;
	uint8_t FatCount;
	uint16_t DirEntriesCount;
	uint16_t TotalSectors;
	uint8_t MediaDescriptorType;
	uint16_t SectorsPerFat;
	uint16_t SectorsPerTrack;
	uint16_t HeadsCount;
	uint8_t HiddenSectorsCount;
	uint8_t LargeSectorCount;

	// extended boot record
	uint8_t DriveNumber;
	uint8_t _Reserved;
	uint8_t Signature;
	uint32_t VolumeId;
	uint8_t	VolumeLabel[11]; // 11 bytes, padded by spaces
	uint8_t	SystemId[8]; // 8 bytes, padded by spaces
		
} BootSector;

typedef struct __attribute__((packed))
{
	uint8_t Name[8];
	uint8_t Extension[3];
	uint8_t Attributes;
	uint8_t _Reserved;
	uint8_t CreationTimeTenths;
	uint16_t CreationTime;
	uint16_t CreationDate;
	uint16_t LastAccessDate;
	uint16_t FirstClusterNumberHigh; // this is always 0 for FAT12 & FAT16
	uint16_t LastModificationTime;
	uint16_t LastModificationDate;
	uint16_t FirstClusterNumberLow;
	uint32_t FileSize;
} DirectoryEntry;


BootSector g_BootSetor;
uint8_t* g_FAT = NULL;
DirectoryEntry* g_RootDirectory = NULL;
uint32_t g_RootDirectoryEnd;

/*
 * Reads in a boot sector from a file and fills it 
 */
bool ReadBootSector(FILE* aBootSectorFile)
{
	return fread(&g_BootSetor, sizeof(g_BootSetor), 1, aBootSectorFile) > 0;
}

/* 
 * Read in _aCount_ sectors at _anLBA_ from _aDisk_ into _aBufferOut_
 */
bool ReadSectors(FILE* aDisk, uint32_t anLBA, uint32_t aCount, void* aBufferOut)
{
	bool ok = true;
	// move to the correct position
	ok = ok && (fseek(aDisk, anLBA * g_BootSetor.BytesPerSector, SEEK_SET) == 0);
	// read the number of times we wanted
	ok = ok && (fread(aBufferOut, g_BootSetor.BytesPerSector, aCount, aDisk) == aCount);

	return ok;
}

/*
 * Read File Allocation Table into memory from _aDisk_
 */
bool ReadFAT(FILE* aDisk)
{
	g_FAT = (uint8_t*) malloc(g_BootSetor.SectorsPerFat * g_BootSetor.BytesPerSector);
	// file allocation table is right after reserved sectors, so thats the lba
	return ReadSectors(aDisk, g_BootSetor.ReservedSectors, g_BootSetor.SectorsPerFat, g_FAT);
}

/*
 * Read root directory struct from _aDisk_
 */
bool ReadRootDirectory(FILE* aDisk)
{
	const uint32_t lba = g_BootSetor.ReservedSectors + (g_BootSetor.SectorsPerFat * g_BootSetor.FatCount);
	const uint32_t size = sizeof(DirectoryEntry) * g_BootSetor.DirEntriesCount;
	uint32_t sectorsCount = size / g_BootSetor.BytesPerSector;
	//round the sectorsCount up
	if(size % g_BootSetor.BytesPerSector > 0) ++sectorsCount;

	g_RootDirectoryEnd = lba + sectorsCount;

	g_RootDirectory = (DirectoryEntry*) malloc(sectorsCount * g_BootSetor.BytesPerSector);
	return ReadSectors(aDisk, lba, sectorsCount, g_RootDirectory);
}

/* 
 * Find a file with _name_ and _ext_ and return a DirEntry for it
 */
DirectoryEntry* FindFile(const char* aName, const char* anExt)
{
	for(uint32_t i = 0; i < g_BootSetor.DirEntriesCount; ++i)
	{
		if(memcmp(aName, g_RootDirectory[i].Name, 8) == 0
			&& memcmp(anExt, g_RootDirectory[i].Extension, 3) == 0)
			return &g_RootDirectory[i];
	}

	return NULL;
}

/*
 * Read file from _aFileEntry_ on _aDisk_ into _aBufferOut_
 */
bool ReadFile(DirectoryEntry* aFileEntry, FILE* aDisk, uint8_t* aBufferOut)
{
	if(aFileEntry == NULL) return false;

	bool success = true;
	uint16_t currentCluster = aFileEntry->FirstClusterNumberLow;
	const uint32_t reservedClusters = 2; 

	do
	{
		const uint32_t lba = g_RootDirectoryEnd + (currentCluster - reservedClusters) * g_BootSetor.SectorsPerCluster;
		success = success && ReadSectors(aDisk, lba, g_BootSetor.SectorsPerCluster, aBufferOut);
		aBufferOut += g_BootSetor.SectorsPerCluster * g_BootSetor.BytesPerSector;

		const uint32_t fatIndex = currentCluster * 3 / 2; // because fat12 clusters are 12 bits
		if(currentCluster % 2 == 0)
			currentCluster = (*(uint16_t*)(g_FAT + fatIndex)) & 0x0FFF; //remove top bits
		else
			currentCluster = (*(uint16_t*)(g_FAT + fatIndex)) >> 4; //remove the bottom bits

	} while(success && currentCluster < 0x0FF8); //0x0ff8 is an end of cluster chain

	return success;
}

int main(int argc, char** argv)
{
	if(argc < 4)
	{ // too few params sent, print error and exit
		printf("ERROR: too few params sent\n");
		printf("Usage: %s <disk_image> <file_name> <file_extension>\n", argv[0]);
		return -1;
	}

	// if the params are correct
	// read the disk
	FILE* disk = fopen(argv[1], "rb");
	if(!disk)
	{
		fprintf(stderr, "Cannot open disk %s\n", argv[1]);
		return -2;
	}

	if(ReadBootSector(disk) == false)
	{
		fprintf(stderr, "Cannot read disk %s\n", argv[1]);
		return -3;
	}

	if(ReadFAT(disk) == false)
	{
		fprintf(stderr, "Cannot read FAT from disk %s\n", argv[1]);
		free(g_FAT);
		return -4;
	}

	if(ReadRootDirectory(disk) == false)
	{
		fprintf(stderr, "Cannot read Root Directory from disk %s\n", argv[1]);
		free(g_FAT);
		free(g_RootDirectory);
		return -5;
	}

	DirectoryEntry* fileEntry = FindFile(argv[2], argv[3]);
	if(fileEntry == NULL)
	{
		fprintf(stderr, "Cannot find file %s.%s\n", argv[2], argv[3]);
		free(g_FAT);
		free(g_RootDirectory);
		return -6;
	}

	uint8_t* fileBuffer = (uint8_t*) malloc(fileEntry->FileSize + g_BootSetor.BytesPerSector);
	if(ReadFile(fileEntry, disk, fileBuffer) == false)
	{
		fprintf(stderr, "Cannot read file %s.%s\n", argv[2], argv[3]);
		free(g_FAT);
		free(g_RootDirectory);
		free(fileBuffer);
		return -7;
	}

	//lets print the file contents
	printf("\n");
	for(size_t i = 0; i < fileEntry->FileSize; ++i)
	{
		if(isprint(fileBuffer[i])) fputc(fileBuffer[i], stdout);
		else printf("<%02x>", fileBuffer[i]);
	}
	printf("\n");

	//cleanup
	free(g_FAT);
	free(g_RootDirectory);
	free(fileBuffer);

	return 0;
}
