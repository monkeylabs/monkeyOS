org 0x0
bits 16

; no matter what we type, start from the main function
start:
	; print our greeting message
	mov si, msg_greeting
	call puts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; in case cpu continues anyway, put in infinite loop
.halt:
	cli
	hlt

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; creating a print function that prints to the screen
; params:
;		ds:si points to string
puts:
	; save any registers we will modify
	push si
	push ax

.puts_loop:
	lodsb ; loads next character in al

	or al, al ; or al with itself to see if next char is null
	jz .puts_done ; if char is 0, string terminate, exit loop

	; actually print the letters
	mov ah, 0x0E ; set the video interrupt mode to TTY
	mov bh, 0	; set the page number to 0
	int 0x10 ; call the interrupt

	jmp .puts_loop	; if next char is not 0, loop

.puts_done:
	; retrieve registers we could have modified
	pop ax
	pop si
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; string definitions
%define ENDL 0x0D, 0x0A

msg_greeting: db "Hello from MonkeyOS Kernel!", ENDL, 0
