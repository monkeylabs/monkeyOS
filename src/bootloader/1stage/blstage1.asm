; we are writing 16 bit code
bits 16 

; tell nasm to place everything next in the entry section
section _ENTRY class=CODE 

; define where the c code starts
extern _cstart_
;export the entry to the outside world
global entry

entry:
	cli
	mov ax, ds ; stack should already be set by stage0, so just copy it here
	mov ss, ax
	mov sp, 0
	mov bp, sp
	sti        ; cli and sti disable interrupts

	; expect boot drive in dl, send it as argument to main
	xor dh, dh
	push dx
	call _cstart_ ; yay lets go to cland

	; if we ever come back from cland, halt the system
	cli
	hlt
