#include "stdio.h"
#include "x86.h"

void putc(char aChar)
{
	x86_Video_WriteCharToTeletype(aChar, 0);
}

void puts(const char* aString)
{
	while(*aString)
	{
		putc(*aString);
		aString++;
	}
}

