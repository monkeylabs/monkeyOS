bits 16

section _TEXT class=CODE

global _x86_Video_WriteCharToTeletype
_x86_Video_WriteCharToTeletype:

	; make new call frame (maybe replace with enter on intel?)
	push bp
	mov bp, sp

	; save bx
	push bx

	; [bp + 0] old call frame
	; [bp + 2] return address (small memory model, 2 bytes offset)
	mov ah, 0Eh
	mov al, [bp + 4] ; [bp + 4] first argument (char)
	mov bh, [bp + 6] ; [bp + 6] second argument (page) 

	int 10h

	; restore bx
	pop bx

	; restore old call frame (maybe replace with leave on intel?)
	mov sp, bp
	pop bp
	ret
