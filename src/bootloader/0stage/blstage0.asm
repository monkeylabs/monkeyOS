org 0x7C00
bits 16

%define ENDL 0x0D, 0x0A

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; FAT12 header
jmp short start
nop

bdb_oem:                   db 'MSWIN4.1' ; oem who formatted the disk
bdb_bytes_per_sector:      dw 512
bdb_sectors_per_cluster:   db 1
bdb_reserved_sectors:      dw 1
bdb_fat_count:             db 2
bdb_dir_entries_count:     dw 0E0h       ; 224
bdb_total_sectors:         dw 2880       ; 2880 * 512 = 1.4MB floppy disk
bdb_media_descriptor_type: db 0F0h       ; F0 = 3.5 floppy disk
bdb_sectors_per_fat:       dw 9
bdb_sectors_per_track:     dw 18
bdb_heads:                 dw 2
bdb_hidden_sectors:        dd 0
bdb_large_sector_count:    dd 0

; extended boot record
ebr_drive_number: db 0                      ; 0x00 floppy drive, 0x80 hdd drive, etc
                  db 0                      ; reserved
ebr_signature:    db 29h
ebr_volume_id:    db 12h, 34h, 56h, 78h     ; serial num
ebr_volume_label: db 'MONKEYOS'             ; 11 bytes padded with spaces
ebr_system_id:    db 'FAT12   '             ; 8 bytes padded with spaces

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; start of actual bootloader code

start:
	; setup our data segments
	mov ax, 0 ; we use ax as we cant write directly into ds, es
	mov ds, ax
	mov es, ax

	; setup our stack
	mov ss, ax
	; stack grows down from where we start it
	; since we start from 0x7C00, anything before that is safe
	mov sp, 0x7C00 

	; some BIOSs might start us off at 07C0:0000 instead of 0000:07C0
	; so better make sure of it
	push es
	push word .after
	retf

.after:
	; print our greeting message
	mov si, msg_greeting
	call puts

	; try and read something from disk
	; BIOS should set dl to drive number
	mov [ebr_drive_number], dl

	; read drive parametres instead of relying on reading them from disk
	push es
	mov ah, 08h
	int 13h
	jc floppy_error
	pop es

	and cl, 0x3F                    ; remove the top 2 bits
	xor ch, ch                      ; ch = 0
	mov [bdb_sectors_per_track], cx ; sector count

	inc dh
	mov [bdb_heads], dh             ; heads count

	; compute lba of root directory
	; lba = ReservedSectors + (SectorsPerFat * FatCount); 
	mov ax, [bdb_sectors_per_fat]
	mov bl, [bdb_fat_count]
	xor bh, bh
	mul bx                          ; ax = sectors per fat * fat count
	add ax, [bdb_reserved_sectors]  ; ax = lba of root dir
	push ax

	; compute size of root dir
	; size = (sizeof(DirectoryEntry) * DirEntriesCount) / BytesPerSector;
	; sizeof(DirectoryEntry) = 32
	mov ax, [bdb_dir_entries_count]
	shl ax, 5                       ; ax *= 2^5
	xor dx, dx                      ; dx = 0
	div word [bdb_bytes_per_sector] ; ax / bytes per sector
	test dx, dx                     ; if dx = 0, we're done
	jz .root_dir_after
	inc ax                          ; if dx != 0, inc number of sectors

.root_dir_after:
	; read the root dir
	mov cl, al                      ; cl = number of sectors to read for root dir
	pop ax                          ; ax = lba of root dir
	mov dl, [ebr_drive_number]      ; should come from bios previously
	mov bx, buffer                  ; es:bx = buffer
	call disk_read

	; now that we can read the disk, time to find blstage1.bin
	xor bx, bx
	mov di, buffer

.search_stage1:
	mov si, file_stage1_bin
	mov cx, 11                      ; size of stage1 file name
	push di
	repe cmpsb                      ; repeat compare bytes for cx times or until unequal
	pop di
	je .found_stage1                ; if equal, found the stage1.bin file

	add di, 32                      ; otherwise, move to next dir entry
	inc bx
	cmp bx, [bdb_dir_entries_count]
	jl .search_stage1

	; stage1 not found
	jmp stage1_not_found_error

.found_stage1:
	; di should have the address to the entry
	mov ax, [di + 26]               ; first logical cluster field (offset 26)
	mov [stage1_cluster], ax

	; load FAT from disk into memory
	mov ax, [bdb_reserved_sectors]
	mov bx, buffer
	mov cl, [bdb_sectors_per_fat]
	mov dl, [ebr_drive_number]
	call disk_read

	; read stage1 and process FAT chain
	mov bx, stage1_LOAD_SEGMENT
	mov es, bx
	mov bx, stage1_LOAD_OFFSET

.load_stage1_loop:
	; Read next cluster
	mov ax, [stage1_cluster]

	; not nice :( hardcoded value
	add ax, 31                          ; first cluster = (stage1_cluster - 2) * sectors_per_cluster + start_sector
	                                    ; start sector = reserved + fats + root directory size = 1 + 18 + 134 = 33
	mov cl, 1
	mov dl, [ebr_drive_number]
	call disk_read

	add bx, [bdb_bytes_per_sector]

	; compute location of next cluster
	mov ax, [stage1_cluster]
	mov cx, 3
	mul cx
	mov cx, 2
	div cx                              ; ax = index of entry in FAT, dx = cluster mod 2

	mov si, buffer
	add si, ax
	mov ax, [ds:si]                     ; read entry from FAT table at index ax

	or dx, dx
	jz .even

.odd:
	shr ax, 4
	jmp .next_cluster_after

.even:
	and ax, 0x0FFF

.next_cluster_after:
	cmp ax, 0x0FF8                      ; end of chain
	jae .read_finish

	mov [stage1_cluster], ax
	jmp .load_stage1_loop

.read_finish:
	; jump to our stage1
	mov dl, [ebr_drive_number]          ; boot device in dl

	mov ax, stage1_LOAD_SEGMENT         ; set segment registers
	mov ds, ax
	mov es, ax

	jmp stage1_LOAD_SEGMENT:stage1_LOAD_OFFSET

	jmp wait_key_and_reboot             ; should never happen

	cli
	hlt

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; creating a print function that prints to the screen
; params:
;		ds:si points to string
puts:
	; save any registers we will modify
	push si
	push ax
	push bx

.puts_loop:
	lodsb ; loads next character in al

	or al, al     ; or al with itself to see if next char is null
	jz .puts_done ; if char is 0, string terminate, exit loop

	; actually print the letters
	mov ah, 0x0E ; set the video interrupt mode to TTY
	mov bh, 0    ; set the page number to 0
	int 0x10     ; call the interrupt

	jmp .puts_loop	; if next char is not 0, loop

.puts_done:
	; retrieve registers we could have modified
	pop bx
	pop ax
	pop si
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; error handlers

floppy_error:
	mov si, msg_read_failed
	call puts
	jmp wait_key_and_reboot

stage1_not_found_error:
	mov si, msg_stage1_not_found
	call puts
	jmp wait_key_and_reboot

wait_key_and_reboot:
	mov ah, 0
	int 16h                     ; wait for keypress
	jmp 0FFFFh:0                ; jump to beginning of BIOS, should reboot

.halt:
	cli                         ; disable interrupts, this way CPU can't get out of "halt" state
	hlt

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; convert LBA access to CHS access
; params
;		ax: LBA address
; returns
;		cx [0-5]: sector number
;		cx [6-15]: cylinder
;		dh: head
lba_to_chs:
	push ax
	push dx
	xor dx, dx                        ; dx = 0
	div word [bdb_sectors_per_track]  ; ax = LBA / SectorsPerTrack, dx = LBA % SectorsPerTrack
	inc dx                            ; dx++
	mov cx, dx                        ; cx = (LBA % SectorsPerTrack) + 1
	xor dx, dx                        ; dx = 0
	div word [bdb_heads]              ; ax = (LBA / SectorsPerTrack) / Heads = cylinder,  dx = (LBA % SectorsPerTrack) % Heads = head
	mov dh, dl                        ; dh = head
	mov ch, al                        ; ch = cylinder
	shl ah, 6
	or cl, ah                         ; put upper 2 bits of cylinder in CL
	pop ax
	mov dl, al
	pop ax
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; params
;		ax LBA address
;		cl number of sectors to read (up to 128)
;		dl drive number
;		es:bx memory address where to store the read data
disk_read:
	push ax
	push bx
	push cx
	push dx
	push di
	push cx	    ; since lba_to_chs will override cx
	call lba_to_chs
	pop ax      ; al = number of sectors to read (from pushing cx)
	mov ah, 02h
	mov di, 3   ; because floppies are unreliable, we should try at least 3 times

.retry:
	pusha				; push all registers
	stc					; set the carry flag to know if it has worked
	int 13h			; read from disk interrupt
	jnc .done

	popa
	call disk_reset
	dec di
	test di, di
	jnz .retry

.fail:				; all 3 attempts are exhausted
	jmp floppy_error

.done:
	popa
	pop di
	pop dx
	pop cx
	pop bx
	pop ax
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; disk_reset
; params
;		dl drive number
disk_reset:
	pusha
	mov ah, 0
	stc
	int 13h
	jc floppy_error
	popa
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; string definitions
msg_greeting: db "Loading MonkeyOS...", ENDL, 0
msg_read_failed: db "disk read failed!", ENDL, 0
msg_stage1_not_found: db "blstage1.bin not found!", ENDL, 0

file_stage1_bin: db "BLSTAGE1BIN"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; stage1 definitions
stage1_cluster: dw 0

stage1_LOAD_SEGMENT     equ 0x2000
stage1_LOAD_OFFSET      equ 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; bios wants the last 2 bytes of the sector to be 0AA55
; as we are emulating loading from a floppy drive, a sector is 512 bytes
; pad with 0s from our last instruction to the end of the sector (minus 2 bytes) 
; $ is the offset of the current line
; $$ is the offset of the current section (ie program in this case)
; so formula for padding is: (Sector Size - 2) - (Program Size (last line - start))
times 510-($-$$) db 0
; now that we are in the correct spot, write the signature
dw 0xAA55

; we will be reading into memory here
buffer:
